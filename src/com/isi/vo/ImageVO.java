package com.isi.vo;

public class ImageVO {
	
	private String model;
	private String imageSize;
	private int picture_x1;
	private int picture_y1;
	private int picture_width;
	private int picture_height;
	private int name_x1;
	private int name_y1;
	private int division_x1;
	private int division_y1;
	private int rank_x1;
	private int rank_y1;
	private int extension_x1;
	private int extension_y1;
	private int org_x1;
	private int org_y1;
	
	public String getModel() {
		return model;
	}
	public ImageVO setModel(String model) {
		this.model = model;
		return this;
	}
	public String getImageSize() {
		return imageSize;
	}
	public ImageVO setImageSize(String imageSize) {
		this.imageSize = imageSize;
		return this;
	}
	public int getPicture_x1() {
		return picture_x1;
	}
	public ImageVO setPicture_x1(int picture_x1) {
		this.picture_x1 = picture_x1;
		return this;
	}
	public int getPicture_y1() {
		return picture_y1;
	}
	public ImageVO setPicture_y1(int picture_y1) {
		this.picture_y1 = picture_y1;
		return this;
	}
	public int getPicture_width() {
		return picture_width;
	}
	public ImageVO setPicture_width(int picture_width) {
		this.picture_width = picture_width;
		return this;
	}
	public int getPicture_height() {
		return picture_height;
	}
	public ImageVO setPicture_height(int picture_height) {
		this.picture_height = picture_height;
		return this;
	}
	public int getName_x1() {
		return name_x1;
	}
	public ImageVO setName_x1(int name_x1) {
		this.name_x1 = name_x1;
		return this;
	}
	public int getName_y1() {
		return name_y1;
	}
	public ImageVO setName_y1(int name_y1) {
		this.name_y1 = name_y1;
		return this;
	}
	public int getDivision_x1() {
		return division_x1;
	}
	public ImageVO setDivision_x1(int division_x1) {
		this.division_x1 = division_x1;
		return this;
	}
	public int getDivision_y1() {
		return division_y1;
	}
	public ImageVO setDivision_y1(int division_y1) {
		this.division_y1 = division_y1;
		return this;
	}
	public int getRank_x1() {
		return rank_x1;
	}
	public ImageVO setRank_x1(int rank_x1) {
		this.rank_x1 = rank_x1;
		return this;
	}
	public int getRank_y1() {
		return rank_y1;
	}
	public ImageVO setRank_y1(int rank_y1) {
		this.rank_y1 = rank_y1;
		return this;
	}
	public int getExtension_x1() {
		return extension_x1;
	}
	public ImageVO setExtension_x1(int extension_x1) {
		this.extension_x1 = extension_x1;
		return this;
	}
	public int getExtension_y1() {
		return extension_y1;
	}
	public ImageVO setExtension_y1(int extension_y1) {
		this.extension_y1 = extension_y1;
		return this;
	}
	public int getOrg_x1() {
		return org_x1;
	}
	public ImageVO setOrg_x1(int org_x1) {
		this.org_x1 = org_x1;
		return this;
	}
	public int getOrg_y1() {
		return org_y1;
	}
	public ImageVO setOrg_y1(int org_y1) {
		this.org_y1 = org_y1;
		return this;
	}
	
	
}
